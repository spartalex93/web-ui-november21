package com.geek.lesson8;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.*;

public class LoginBlock {

    private SelenideElement loginFrame = $(By.xpath("//iframe[contains(@src,'rambler.ru/login')]"));

    @Step("Переключить в iframe для логина")
    public LoginBlock switchToLoginFrame() {
        switchTo().frame(loginFrame);
        return this;
    }

    private SelenideElement loginInput = $(By.id("login"));

    @Step("Заполнить логин")
    public LoginBlock fillLoginInput(String login) {
        loginInput.sendKeys(login);
        return this;
    }

    private SelenideElement passwordInput = $(By.id("password"));

    @Step("Заполнить пароль")
    public LoginBlock fillPasswordInput(String password) {
        passwordInput.sendKeys(password);
        return this;
    }

    private SelenideElement sumbitLoginButton = $(By.xpath("//span[.='Войти']"));

    @Step("Нажать на кнопку войти")
    public MainPage submitLogin() {
        sumbitLoginButton.click();
        return page(MainPage.class);
    }
}

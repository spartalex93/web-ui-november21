package com.geek.lesson8;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class MainPage {
    private SelenideElement loginButton = $(By.xpath("//button[.='Войти']"));

    @Step("Клик на кнопку Логин")
    public MainPage clickLoginButton() {
        loginButton.click();
        return this;
    }

    private ElementsCollection moviesList =
            $$(By.xpath("//a[contains(@href, 'movie') and contains(@data-test,'LINK')]/ancestor::div[@data-test='ITEM']"));

    @Step("Клик на фильм по имени")
    public MoviePage clickToMovieByName(String filmName) {
        moviesList.findBy(Condition.text(filmName)).click();
        return page(MoviePage.class);
    }
}

package com.geek.lesson8;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class MoviePage {
    private SelenideElement likeButton =
            $(By.xpath("//section[@data-test='PAGE-SECTION TITLE-SECTION']//button[@data-test='BUTTON FAVORITE']"));

    @Step("Добавить фильм в избранное")
    public MoviePage likeFilm() {
        likeButton.click();
        return this;
    }

    private SelenideElement addedToFavouritesElement = $(By.xpath("//div[.='Добавлено в избранное']"));

    @Step("Проверить наличие сообщения об успешном добавлении фильма в избранное")
    public MoviePage checkAddedToFavouritesElementIsDisplayed() {
        addedToFavouritesElement.shouldBe(visible);
        return this;
    }
}

package com.geek.lesson3;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static ru.yandex.qatools.htmlelements.matchers.WebElementMatchers.isDisplayed;

public class AfishaTest {
    public static void main(String[] args) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class);
        driver.get("https://afisha.ru");
        login(driver, webDriverWait);

        webDriverWait.until(d -> d.findElements(
                By.xpath("//a[contains(@href, 'movie') and contains(@data-test,'LINK')]/ancestor::div[@data-test='ITEM']")).size() > 0);
        List<WebElement> filmsList = driver.findElements(
                By.xpath("//a[contains(@href, 'movie') and contains(@data-test,'LINK')]/ancestor::div[@data-test='ITEM']"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
                filmsList.stream().filter(f -> f.getText().contains("355")).findFirst().get());
        filmsList.stream().filter(f -> f.getText().contains("355")).findFirst().get().click();
        //filmsList.stream().filter(f -> f.getCssValue("color").contains("Gucci")).findFirst().get().click();
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//section[@data-test='PAGE-SECTION TITLE-SECTION']//button[@data-test='BUTTON FAVORITE']")));
        driver.findElement(By.xpath("//section[@data-test='PAGE-SECTION TITLE-SECTION']//button[@data-test='BUTTON FAVORITE']")).click();
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[.='Добавлено в избранное']")));
        assertThat(driver.findElement(By.xpath("//div[.='Добавлено в избранное']")), isDisplayed());
        driver.quit();
    }

    static void login(WebDriver driver, WebDriverWait webDriverWait) throws InterruptedException {
        driver.findElement(By.xpath("//button[.='Войти']")).click();
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@src,'rambler.ru/login')]")));
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login")));
        driver.findElement(By.id("login")).sendKeys("spartalex1993");
        driver.findElement(By.id("password")).sendKeys("Test4test");
        webDriverWait.until(d -> d.findElement(By.id("login")).getAttribute("value").contains("@rambler.ru"));
        driver.findElement(By.xpath("//span[.='Войти']")).click();
        Thread.sleep(20000);
    }
}

package com.geek.lesson6;

import com.geek.lesson7.CustomLoggerNew;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.events.EventFiringDecorator;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.Iterator;

@Story("Фильмы")
public class AfishaTest {
    WebDriver driver;
    EventFiringWebDriver eventFiringWebDriver;
    MainPage mainPage;
    LoginBlock loginBlock;
    private final static String AFISHA_BASE_URL = "https://afisha.ru";

    @BeforeAll
    static void registerDriver() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupDriver() {
        //eventFiringWebDriver = new EventFiringWebDriver(new ChromeDriver());
        //eventFiringWebDriver.register(new CustomLogger());

        driver = new EventFiringDecorator(new CustomLoggerNew()).decorate(new ChromeDriver());
        mainPage = new MainPage(driver);
        loginBlock = new LoginBlock(driver);
        driver.get(AFISHA_BASE_URL);
    }

    @Test
    @Description("Тест добавления фильма в избранное")
    @TmsLink("123")
    @Feature("Избранное")
    void likeRandomMovieTest() throws InterruptedException {
        //mainPage.loginButton.click();
        //
        //driver.switchTo().frame(loginBlock.loginFrame);
        //loginBlock.loginInput.sendKeys("spartalex1993");
        //loginBlock.passwordInput.sendKeys("Test4test");

        new MainPage(driver).clickLoginButton();

        new LoginBlock(driver)
                .switchToLoginFrame()
                .fillLoginInput("spartalex1993")
                .fillPasswordInput("Test4test")
                .submitLogin();
        Thread.sleep(1000);

        new MainPage(driver)
                .clickToMovieByName("Матрица");

        new MoviePage(driver)
                .likeFilm()
                .checkAddedToFavouritesElementIsDisplayed();
    }

    @AfterEach
    void tearDown() {
        LogEntries logs = driver.manage().logs().get(LogType.BROWSER);
        Iterator<LogEntry> iterator = logs.iterator();

        while (iterator.hasNext()) {
            Allure.addAttachment("Элемент лога браузера", iterator.next().getMessage());
        }
        driver.quit();
    }
}

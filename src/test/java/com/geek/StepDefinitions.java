package com.geek;

import com.geek.lesson8.LoginBlock;
import com.geek.lesson8.MainPage;
import com.geek.lesson8.MoviePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static com.codeborne.selenide.Selenide.open;

public class StepDefinitions {
    @Given("Пользователь авторизован на сайте")
    public void userAuthorization() throws InterruptedException {
        open("https://afisha.ru");
        new MainPage().clickLoginButton();
        new LoginBlock()
                .switchToLoginFrame()
                .fillLoginInput("spartalex1993")
                .fillPasswordInput("Test4test")
                .submitLogin();
        Thread.sleep(20000);
    }

    @When("Я кликаю на фильм")
    public void iClickRandomFilm() {
        new MainPage().clickToMovieByName("пес");
    }

    @And("Я добавляю фильм в избранное")
    public void iAddFilmToFavourites() {
        new MoviePage().likeFilm();
    }

    @Then("Я вижу сообщение об успешном добавлении фильма в избранное")
    public void iSeeSuccessMessage() {
        new MoviePage().checkAddedToFavouritesElementIsDisplayed();
    }

    @When("Я кликаю {string} на фильм")
    public void iClickFilmByName(String name) {
        new MainPage().clickToMovieByName(name);
    }
}
